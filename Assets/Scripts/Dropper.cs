using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dropper : MonoBehaviour
{
    MeshRenderer Renderer;
    Rigidbody Rigidbody;
    [SerializeField] float timer = 3f;
    void Start()
    {
        Renderer = GetComponent<MeshRenderer>();
        Rigidbody = GetComponent<Rigidbody>();
        
        Renderer.enabled = false;
        Rigidbody.useGravity = false;
    }

    void Update()
    {
        if (timer < Time.time){
            Renderer.enabled = true;
            Rigidbody.useGravity = true;
        }
    }
}
