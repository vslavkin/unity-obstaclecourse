using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scorer : MonoBehaviour
{
    int hits = 0;
    private void OnCollisionEnter(Collision other) {
        if(!other.gameObject.GetComponent<ObjectHit>()){return;}
        if(other.gameObject.GetComponent<ObjectHit>().getTouched()){return;}
        hits++;
        Debug.Log("Chocaste " +hits+ " veces");
        other.gameObject.GetComponent<ObjectHit>().Touched();
    }
}
