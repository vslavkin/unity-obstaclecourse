using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    [SerializeField]float Speed;
    Rigidbody rb;
    [SerializeField] float CooldownT;
    [SerializeField] float Multiplier;
    float CooldownBackup;
    bool Recharged = true;

    [SerializeField] GameObject Slider;

    [SerializeField] ParticleSystem SmokeParticles;



    


    void Start()
    {
       rb = GetComponent<Rigidbody>();
       CooldownBackup = CooldownT;
       Slider.GetComponent<CoolDown>().SetMax(Mathf.RoundToInt(CooldownBackup*100));
    }
    void FixedUpdate()
    {
        rb.angularVelocity = new Vector3 (rb.angularVelocity.x, 0f, rb.angularVelocity.y);
        Timer();
        movePlayer();
    }

    private void movePlayer(){
        float xValue = Input.GetAxis("Horizontal"); //* Time.deltaTime * Speed;
        float zValue = Input.GetAxis("Vertical"); //* Time.deltaTime * Speed;
        Vector3 Mvector = new Vector3(xValue, 0f, zValue);
        Vector3 MovementVector = transform.TransformDirection(Mvector) * Speed * Time.fixedDeltaTime;
        rb.velocity = new Vector3(MovementVector.x, rb.velocity.y, MovementVector.z);

    }

    private void Timer(){
        if (Recharged){return;}
        CooldownT -= Time.deltaTime;
        if(Slider.GetComponent<CoolDown>()){
            Slider.GetComponent<CoolDown>().SetActual(Mathf.RoundToInt(CooldownT*100));
        }
        if (CooldownT <= 0){
            Recharged = true;
            CooldownT = CooldownBackup;
        }
    }


    public void Teleport(){
        if (Recharged){
            float xValue = Input.GetAxis("Horizontal"); //* Time.deltaTime * Speed;
            float zValue = Input.GetAxis("Vertical"); //* Time.deltaTime * Speed;
            Vector3 Mvector = new Vector3(xValue, 0f, zValue);
            if(Mvector != new Vector3(0,0,0)) {
                var teleportPoint = transform.position + Mvector * Multiplier;
                Debug.DrawRay(transform.position,(teleportPoint - transform.position), Color.red,3);
                Raycast(teleportPoint);
            }
        }
    }

    void Raycast(Vector3 teleportPoint){ 
        RaycastHit hit;
        Ray teleportRay = new Ray(transform.position, (teleportPoint - transform.position).normalized);
        if (Physics.Raycast(teleportRay, out hit, Vector3.Distance(transform.position, teleportPoint))){
            if(hit.collider.tag == "Wall"){
                return;
            }
        }
        if (hit.collider){
            if (isInside(hit.collider, teleportPoint)) {
                return;
                }
        }
        Instantiate(SmokeParticles, transform.position, Quaternion.identity);
        Instantiate(SmokeParticles, teleportPoint, Quaternion.identity);
        rb.MovePosition(teleportPoint);
        Recharged = false;
    }

    private bool isInside(Collider c, Vector3 teleportPoint){
        Vector3 closest = c.ClosestPoint(teleportPoint);
        Vector3 distance = teleportPoint - closest;
        Vector3 squareS = GetComponent<Collider>().bounds.size;
        if(distance.x > squareS.x & distance.z>squareS.z){Debug.Log("escala");return true;}
        return (closest == teleportPoint);
    }


}
