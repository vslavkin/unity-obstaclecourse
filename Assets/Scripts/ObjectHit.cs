using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectHit : MonoBehaviour
{
    [SerializeField] Color touchedColor;
    [SerializeField] bool  istouched;

    private void OnCollisionEnter(Collision other) {
        if(other.gameObject.tag == "Player"){
            GetComponent<MeshRenderer>().material.color = touchedColor;
        }       
    }
    public bool getTouched(){
        return istouched;
    }

    public void Touched(){
        istouched = true;
    }


}